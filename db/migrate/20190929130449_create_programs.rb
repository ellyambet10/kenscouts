class CreatePrograms < ActiveRecord::Migration[5.2]
  def change
    create_table :programs do |t|
      t.string :morn_insp
      t.string :even_ins
      t.string :night_ins

      t.timestamps
    end
  end
end
