class CreateStudents < ActiveRecord::Migration[5.2]
  def change
    create_table :students do |t|
      t.string :name
      t.integer :code
      t.string :school
      t.boolean :gender
      t.string :location
      t.boolean :disability

      t.timestamps
    end
  end
end
