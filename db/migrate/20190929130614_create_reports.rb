class CreateReports < ActiveRecord::Migration[5.2]
  def change
    create_table :reports do |t|
      t.string :student_name
      t.integer :maths_mark
      t.integer :eng_mark
      t.integer :kis_mark

      t.timestamps
    end
  end
end
