Rails.application.routes.draw do
  resources :reports
  resources :schools
  resources :programs
  resources :students

  root 'students#index'
end
