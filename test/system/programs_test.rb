require "application_system_test_case"

class ProgramsTest < ApplicationSystemTestCase
  setup do
    @program = programs(:one)
  end

  test "visiting the index" do
    visit programs_url
    assert_selector "h1", text: "Programs"
  end

  test "creating a Program" do
    visit programs_url
    click_on "New Program"

    fill_in "Even ins", with: @program.even_ins
    fill_in "Morn insp", with: @program.morn_insp
    fill_in "Night ins", with: @program.night_ins
    fill_in "Student", with: @program.student_id
    click_on "Create Program"

    assert_text "Program was successfully created"
    click_on "Back"
  end

  test "updating a Program" do
    visit programs_url
    click_on "Edit", match: :first

    fill_in "Even ins", with: @program.even_ins
    fill_in "Morn insp", with: @program.morn_insp
    fill_in "Night ins", with: @program.night_ins
    fill_in "Student", with: @program.student_id
    click_on "Update Program"

    assert_text "Program was successfully updated"
    click_on "Back"
  end

  test "destroying a Program" do
    visit programs_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Program was successfully destroyed"
  end
end
