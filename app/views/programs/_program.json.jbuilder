json.extract! program, :id, :morn_insp, :even_ins, :night_ins, :student_id, :created_at, :updated_at
json.url program_url(program, format: :json)
