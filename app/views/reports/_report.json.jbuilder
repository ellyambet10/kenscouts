json.extract! report, :id, :student_name, :maths_mark, :eng_mark, :kis_mark, :school_id, :student_id, :created_at, :updated_at
json.url report_url(report, format: :json)
