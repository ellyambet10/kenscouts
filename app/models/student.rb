class Student < ApplicationRecord
  has_many :reports
  has_many :programs
end
