class Report < ApplicationRecord
  belongs_to :school
  belongs_to :student
end
